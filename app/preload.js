// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.
window.addEventListener('DOMContentLoaded', () => {
    const replaceText = (selector, text) => {
        const element = document.getElementById(selector)
        if (element) element.innerText = text
    }

    for (const type of ['chrome', 'node', 'electron']) {
        replaceText(`${type}-version`, process.versions[type])
    }
})


//------------------------------------------------------

require('dotenv').config()
const DB = require('./database')


// SSHConnection.then((db) => {
//     db.query("SELECT * FROM posts", function (err, result, fields) {
//         if (err) throw err;
//         console.log(result);
//     });
// })

// реакция на событие
let observer = new MutationObserver(mutations => {
    mutations.forEach(function(mutation){
        mutation.addedNodes.forEach(function(node){
            // console.log('event !')

            console.log(node)
        });
    });
});
// ui-state-highlight
// отслеживаем соббытия
function addObserverIfDesiredNodeAvailable() {
    let content = document.getElementById('content')
    if (!content) {
        window.setTimeout(addObserverIfDesiredNodeAvailable, 500);
        return;
    }
    var config = {
        childList: true,
        attributes: true,
        subtree: true,
        characterData: true,
    };
    observer.observe(content, config);
}
addObserverIfDesiredNodeAvailable();

// observer.observe(document.getElementById('tree'), {
//     childList: true, // наблюдать за непосредственными детьми
//     subtree: true, // и более глубокими потомками
//     characterDataOldValue: true // передавать старое значение в колбэк
// });

window.addEventListener('DOMContentLoaded', (e) => {
    let show_tables = document.getElementById('show_tables')
    let tree = document.getElementById('tree')
    show_tables.onclick = () => {
        tree.innerHTML = ""


        DB.then((db) => {
            db.query("SHOW TABLES", function (err, result, fields) {
                if (err) throw err;
                result.forEach((item) => {
                    // console.log(item.Tables_in_exporter);

                    // создать div
                    var div = document.createElement("div");
                    // добавить id
                    div.setAttribute('id', item.Tables_in_exporter)
                    div.setAttribute('class', 'draggable')
                    // добавить елемент в список
                    tree.append(div)
                    // добавить в елемент строку
                    div.append(item.Tables_in_exporter)

                    // // создать перенос br
                    // var br = document.createElement("br");
                    // // добавить в список
                    // tree.appendChild(br);
                })
            });
        })
    };
    // console.log(e)
})